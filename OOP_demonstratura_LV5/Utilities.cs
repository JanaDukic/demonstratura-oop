﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP_demonstratura_LV5
{
    public static class Utilities
    {
        public static int CountSpikes<T>(T[] polje) where T : IComparable<T>
        {
            int counter = 0;
            for (int i = 1; i < polje.Length - 2; i++)
            {
                if (polje[i].CompareTo(polje[i + 1]) > 0 && polje[i].CompareTo(polje[i - 1]) > 0)
                {
                    counter++;
                }
            }
            return counter;
        }
    }

    public class VremenskaPrognoza
    {
        private double temperature;
        private string description;

        public VremenskaPrognoza(double temperatura)
        {
            this.temperature = temperatura;
            this.description = "default opis";
        }

        public double getTemp { get { return this.temperature; } }
        public string getDesc { get { return this.description; } }

        public override string ToString()
        {
            return "Temperatura iznosi: " + this.temperature + " , i prognoza: " + this.description;
        }
    }

    public class Example2
    {
        public double[] TestMethod()
        {
            Console.WriteLine("Unesite broj elemenata polja");
            int broj = int.Parse(Console.ReadLine());
            double[] poljeTemp = new double[broj] ;
            Console.WriteLine("Unesite brojeve, u C# se double vrijednost unosi sa zarezom!");
            for(int i=0;i < broj; i++)
            {
                poljeTemp[i] = double.Parse(Console.ReadLine());
          
            }
            return poljeTemp;
        }
    }
}