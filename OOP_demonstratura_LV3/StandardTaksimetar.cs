﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP_demonstratura_LV3
{
    public class StandardTaksimetar : Taksimetar
    {
        private int cijenaStarta;

        public StandardTaksimetar(int price, int cijenazastart): base(price)
        {
            this.cijenaStarta = cijenazastart;
        }

        public int CijenaStarta
        {
            get { return this.cijenaStarta; }
        }

        public override int CalculatePriceOfDriving(int km)
        {
            return this.cijenaStarta + km * this.CijenaPoKm; 
        }
    }
}
