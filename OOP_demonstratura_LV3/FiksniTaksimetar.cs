﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP_demonstratura_LV3
{
    public class FiksniTaksimetar: Taksimetar
    {
        private int fiksaniznos;
        private int brkmZaTajIznos;

        public FiksniTaksimetar(int price, int fiksno, int brojkilometarazafiksniiznos): base(price)
        {
            this.fiksaniznos = fiksno;
            this.brkmZaTajIznos = brojkilometarazafiksniiznos;
        }

        public int Fiksaniznos
        {
            get { return this.fiksaniznos; }
        }
        public int BrKmZaTajIznos
        {
            get { return this.brkmZaTajIznos; }
        }

        public override int CalculatePriceOfDriving(int km)
        {
            int rezultat = 0;
            if( km < this.brkmZaTajIznos)
            {
                rezultat = this.fiksaniznos;
            }
            else
            {
                rezultat = km * this.CijenaPoKm;
            }
            return rezultat;
        }
    }
}
