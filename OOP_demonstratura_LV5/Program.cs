﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OOP_demonstratura_LV5.Test;
namespace OOP_demonstratura_LV5
{
    class Program
    {
        static void Main(string[] args)
        {
            //prvi zadatak
            Example primjer = new Example();
            primjer.RunSimpleExample();

            //drugi zadatak
            Example1 primjer1 = new Example1();
            primjer1.SimpleExample1();

            //treci zadatak
            int[] polje = new int[]{ 5, 3, 5, 4, 1, 7 };
            Console.WriteLine(Utilities.CountSpikes(polje));
            Example2 primjer2 = new Example2();
            Console.WriteLine(Utilities.CountSpikes(primjer2.TestMethod()));

        }
    }
}
