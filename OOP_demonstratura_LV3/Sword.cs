﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP_demonstratura_LV3
{
    public class Sword: ItemInVideogame
    {
        private double snaga;

        public Sword(string name, double weight, double value, double strenght):base(name, weight, value)
        {
            this.snaga = strenght;
        }
        public double Snaga
        {
            get { return this.snaga; }
        }
        public override string ToString()
        {
            return base.ToString() + " Sword power: " + this.snaga;
        }
    }
}
