﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP_demonstratura_LV5.LinearAlgebra
{
    public class Vector
    {
        public double i {get;   set; }
        public double j { get;  set; }
        public double k { get;  set; }

        public Vector()
        {
            this.i = 0.0;
            this.j = 0.0;
            this.k = 0.0;
        }

        public Vector(double I, double J, double K)
        {
            this.i = I;
            this.j = J;
            this.k = K;
        }

        public override string ToString()
        {
            return "Vektor: (" + this.i + " , " + this.j + " , " + this.k + " )";  
        }

        public double EuclidsMethode()
        {
            double rez = System.Math.Sqrt(System.Math.Pow(this.i, 2) + System.Math.Pow(this.j, 2) + System.Math.Pow(this.k, 2));
            return rez;
        }
    }
}

namespace OOP_demonstratura_LV5.LinearAlgebra.Math
{
    public static class LinearAlgebraMath
    {
        public static Vector DotProduct(Vector vektor1, Vector vector2)
        {
            double prvi = vektor1.i * vector2.i;
            double drugi = vektor1.j * vector2.j;
            double treci = vektor1.k * vector2.k;
            return new Vector(prvi, drugi, treci);
        }

        public static double CalculateAngular(Vector vektor1, Vector vektor2)
        {
            //ili ako zori zeli da se ovdje koristi dotproduct
            //Vector vektor = DotProduct(vektor1, vektor2);
            //double brojnik = vektor.i + vektor.j + vektor.k;
            double brojnik = vektor1.i * vektor2.i + vektor1.j * vektor2.j + vektor1.k * vektor2.k;
            double prvi_u_naz = System.Math.Sqrt(System.Math.Pow(vektor1.i, 2) + System.Math.Pow(vektor1.j, 2) + System.Math.Pow(vektor1.k, 2));
            double drugi_u_naz = System.Math.Sqrt(System.Math.Pow(vektor2.i, 2) + System.Math.Pow(vektor2.j, 2) + System.Math.Pow(vektor2.k, 2));
            double nazivnik = prvi_u_naz * drugi_u_naz;
            double rez = System.Math.Acos(brojnik/nazivnik);
            return rez;
        }
    }
}


namespace OOP_demonstratura_LV5.Test
{
    using OOP_demonstratura_LV5.LinearAlgebra;
    using OOP_demonstratura_LV5.LinearAlgebra.Math;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class Example 
    {
        public void RunSimpleExample()
        {
            Vector vektor1 = new Vector(2, 4, 5);
            Vector vektor2 = new Vector(3, 4, 6);
            vektor1.i = 13;
            vektor2.i = 17;
            System.Console.WriteLine(vektor1);
            System.Console.WriteLine(vektor2);
            Vector vectorsproduct = LinearAlgebraMath.DotProduct(vektor1, vektor2);
            System.Console.WriteLine("produkt 2 vektora = " + vectorsproduct);
            System.Console.WriteLine("kut u radijanima : " + LinearAlgebraMath.CalculateAngular(vektor1, vektor2));
        }
    }
}