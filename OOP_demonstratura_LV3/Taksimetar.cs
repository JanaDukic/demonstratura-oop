﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP_demonstratura_LV3
{
    public abstract class Taksimetar
    {
        private int cijenaPoKm;

        public Taksimetar(int cijena)
        {
            this.cijenaPoKm = cijena;
        }

        public int CijenaPoKm
        {
            get { return this.cijenaPoKm; }
        }

        public bool IsANightDrive(DateTime time)
        {
            if(time.Hour > 20 && time.Hour < 6)
            {
                return true;
            }
            return false;
        }

        public abstract int CalculatePriceOfDriving(int km);
        
       
    }
}
