﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP_demonstratura_LV3
{
    public class CalculatePointsInGame
    {
        private int brojBodova;
        private int n;
        //parametarski
        public CalculatePointsInGame(int brbodova, int brojznakova)
        {
            this.brojBodova= brbodova;
            this.n = brojznakova;
        }

        public virtual int CalulatePointsFromWord(string rijec)
        {
            int counter = 0;
            int rez = 0;
            for(int i=0; i<rijec.Length; i++)
            {
                counter++;
                if(counter == this.n)
                {
                    counter = 0;
                   rez  =  rez + this.brojBodova;
                   
                }

            }
            return rez;
        }
    }
}
