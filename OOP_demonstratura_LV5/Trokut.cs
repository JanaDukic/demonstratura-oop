﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP_demonstratura_LV5
{
    public class Trokut
    {
        private double a;
        private double b;
        private double c;

        public Trokut()
        {
            this.a = 1;
            this.b = 1;
            this.c = 1;
        }

        public Trokut(double A, double B, double C)
        {
            if (isTriangle(A, B, C)) { 
                this.a = A;
                this.b = B;
                this.c = C;
            }
            else
            {
                throw new GeometryException(A,B,C, "This lenghts cannot create triangle: ");
            }
        }

        public double GetA { get { return this.a; } }
        public double GetB { get { return this.b; } }
        public double GetC { get { return this.c; } }

        public override string ToString()
        {
            return "Trokut: " + this.a + ", " + this.b + ", " + this.c;
        }

        public double CalculateOpseg(Trokut trokut)
        {
            return a + b + c;
        }

        public double CalculatePovrsina(Trokut trokut)
        {
            double s = (trokut.a + trokut.b + trokut.c) / 2;
            double P = System.Math.Sqrt(s * (s - trokut.a) * (s - trokut.b) * (s - trokut.c));
            return P;
        }

        public bool isPravokutan(Trokut trokut)
        {
            double a = trokut.a;
            double b = trokut.b;
            double C = trokut.c;
            if(a*a + b*b == c * c)
            {
                return true;
            }
            return false;
        }
        public bool isTriangle(double a, double b, double c)
        {
            if(a + b > c && a + c > b && b +c > a)
            {
                return true;
            }
            return false;

        }
    }

    public class GeometryException : Exception
    {
        private double a;
        private double b;
        private double c;
        public double getA { get { return this.a; } }
        public double GetB { get { return this.b; } }
        public double GetC { get { return this.c; } }

        public GeometryException(double A, double B, double C, string message):base(message)
        {
            this.a = A;
            this.b = B;
            this.c = C;
        }

    }

    public class Example1
    {
        public void SimpleExample1()
        {
            try
            {
                Trokut jednakostranican = new Trokut(1, 1, 1);
                
            }
            catch (GeometryException e)
            {
                Console.WriteLine(e.Message + e.getA + ", " + e.GetB + ", " + e.GetC);
            }

            try
            {
                Trokut jednakokracan = new Trokut(4, 4, 2);

            }
            catch (GeometryException e)
            {
                Console.WriteLine(e.Message + e.getA + ", " + e.GetB + ", " + e.GetC);
            }

            try
            {
                Trokut pravokutan = new Trokut(3, 4, 5);

            }
            catch (GeometryException e)
            {
                Console.WriteLine(e.Message+ e.getA + ", " + e.GetB + ", " + e.GetC);
            }

            try
            {
                Trokut nemoguc = new Trokut(1, 2, 3);

            }
            catch (GeometryException e)
            {
                Console.WriteLine(e.Message + e.getA + ", " + e.GetB + ", " + e.GetC);
            }


            
           
        }

    }
}
