﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP_demonstratura_LV3
{
    public class CalculateAccordingToChar : CalculatePointsInGame
    {
        private int bodSamoglasnici;
        private int bodSuglasnici;

        public CalculateAccordingToChar(int brbodova, int n, int samoglasnik, int suglasnik):base(brbodova, n)
        {
            this.bodSamoglasnici = samoglasnik;
            this.bodSuglasnici = suglasnik;
        }
        public override int CalulatePointsFromWord(string rijec)
        {
            int br = 0;
            for(int i= 0; i< rijec.Length; i++)
            {
                
                if(rijec[i]=='a' || rijec[i] == 'e'  || rijec[i] == 'i' || rijec[i] == 'o'  || rijec[i] == 'u')
                {
                    br = br + bodSamoglasnici;
                    
                }
                else
                {
                    br = br + bodSuglasnici;
                   
                }
            }
            return br + base.CalulatePointsFromWord(rijec);
        }
    }
}
