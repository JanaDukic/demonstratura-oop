﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP_demonstratura_LV3
{
    public class Shield: ItemInVideogame
    {
        private double razinaOklopa;

        public Shield(string name, double weight, double value, double razinaoklopa): base(name, weight, value)
        {
            this.razinaOklopa = razinaoklopa;
        }

        public double RazinaOklopa
        {
            get { return this.razinaOklopa; }
        }

        public override string ToString()
        {
            return base.ToString() + " Shield level: " + this.razinaOklopa ;
        }
    }
}
