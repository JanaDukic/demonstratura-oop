﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP_demonstratura_LV3
{
    public class ItemInVideogame
    {
        private string naziv;
        private double tezina;
        private double vrijednost;

        //parametarski
        public ItemInVideogame(string name, double weight, double value)
        {
            this.naziv = name;
            this.tezina = weight;
            this.vrijednost = value;
        }

        public string Naziv
        {
            get { return this.naziv; }
        }
        public double Tezina
        {
            get { return this.tezina; }
        }
        public double Vrijednost
        {
            get { return this.vrijednost; }
        }

        public override string ToString()
        {
            return "Item name: " + this.naziv + " Weights: " + this.tezina + " Value: " + this.vrijednost;
        }

    }
}
